{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<footer id="footer"  class="container">
						<div class="row">{$HOOK_FOOTER}</div>
					</footer>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
<script type='text/javascript'>
(function() {
var s = document.createElement('script');s.type='text/javascript';s.async=true;s.id='lsInitScript';
s.src='https://livesupporti.com/Scripts/clientAsync.js?acc=96cc26db-91cc-4185-abf6-e2291a9699ac&skin=Modern';
var scr=document.getElementsByTagName('script')[0];scr.parentNode.appendChild(s, scr);
})();
</script>
<script src="https://app2.salesmanago.pl/dynamic/ki6ajdbbbq36psjo/popups.js"></script>
<script type="text/javascript" src="/sw.js"></script>
<script type="text/javascript">sm('webPush', ['ad8bc2f4-7b90-4b22-95ff-7fa104b3585c', '72']);</script>;
	</body>

</html>