<?php
Class Media extends MediaCore
{
	/*
	* module: plpganalytics
	* date: 2015-08-27 15:13:42
	* version: 1.8.3
	*/
	public static function deferScript($matches)
	{
		$new_pattern_js = '#\s*(<\s*script(?:\s[^>]*(?:javascript)(?!.*data-keepinline)[^>]*|)+>)(.*)(<\s*\/script\s*[^>]*>)\s*#Uims';
		if (!is_array($matches))
			return false;
		$inline = '';
		if (isset($matches[0]))
			$original = trim($matches[0]);
		if (isset($matches[2]))
			$inline = trim($matches[2]);
		
		if (!empty($inline) && preg_match($new_pattern_js, $original) !== 0 && Media::$inline_script[] = $inline)
			return '';
		
		preg_match('/src\s*=\s*["\']?([^"\']*)[^>]/ims', $original, $results);
		if (array_key_exists(1, $results))
		{
			if (substr($results[1], 0, 2) == '//')
			{
				$protocol_link = Tools::getCurrentUrlProtocolPrefix();
				$results[1] = $protocol_link.ltrim($results[1], '/');
			}
			if (in_array($results[1], Context::getContext()->controller->js_files) || in_array($results[1], Media::$inline_script_src))
				return '';
		}
		
		return $original;
	}
}