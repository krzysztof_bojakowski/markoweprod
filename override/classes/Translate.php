<?php
class Translate extends TranslateCore
{
    /*
    * module: allegro
    * date: 2017-09-27 22:52:04
    * version: 4.1.0.8
    */
    public static function getModuleTranslation($module, $string, $source, $sprintf = null, $js = false)
    {
        $str = parent::getModuleTranslation($module, $string, $source, $sprintf, $js);
       if (($source == 'AdminAllegroProductcontroller' ||
            $source == 'AdminAllegroFieldcontroller' ||
            $source == 'AdminAllegroThemecontroller' ||
            $source == 'AdminAllegroAccountcontroller' ||
            $source == 'AdminAllegroAuctioncontroller' ||
            $source == 'AdminAllegroSynccontroller' ||
            $source == 'AdminAllegroPreferencescontroller') 
        && $str == htmlentities($string)) {
            return parent::getModuleTranslation($module, $string, 'ParentAllegrocontroller', $sprintf, $js);
       } else {
            return $str;
       }
    }
}