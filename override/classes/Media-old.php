<?php
Class Media extends MediaCore
{
	public static function deferScript($matches)
	{
		if (!is_array($matches))
			return false;
		$inline = '';

		if (isset($matches[0]))
			$original = trim($matches[0]);

		if (isset($matches[1]))
			$inline = trim($matches[1]);

		/* This is an inline script, add its content to inline scripts stack then remove it from content */
		if (!empty($inline) && preg_match('/<\s*script(?!.*data-keepinline)[^>]*>/ims', $original) !== 0 && Media::$inline_script[] = $inline)
			return '';

		/* This is an external script, if it already belongs to js_files then remove it from content */

		preg_match('/src\s*=\s*["\']?([^"\']*)[^>]/ims', $original, $results);
		if (isset($results[1]) && (in_array($results[1], Context::getContext()->controller->js_files) || in_array($results[1], Media::$inline_script_src)))
			return '';

		/* return original string because no match was found */
		return $original;
	}
}
