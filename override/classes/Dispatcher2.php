<?php
class Dispatcher extends DispatcherCore
{
    private static function _isPageCacheActive()
    {
        if (self::$_is_page_cache_active == -1)
        {
            if (file_exists(dirname(__FILE__).'/../../modules/pagecache/pagecache.php'))
            {
                require_once(dirname(__FILE__).'/../../modules/pagecache/pagecache.php');
                self::$_is_page_cache_active = Module::isEnabled('pagecache');
            } else {
                Logger::addLog('Page cache has not been well uninstalled, please, remove manually the following functions in file '.__FILE__.': _isPageCacheActive(), dispatch(), dispatch_15() and dispatch_16(). If you need help contact our support.', 4);
                return false;
            }
        }
        return self::$_is_page_cache_active;
    }
    public function getControllerFromURL($url, $id_shop = null) {
        $controller = false;
        $is_fc_module = false;
        if (isset(Context::getContext()->shop) && $id_shop === null)
            $id_shop = (int)Context::getContext()->shop->id;
        $query = parse_url($url, PHP_URL_QUERY);
        if ($query) {
            $query = html_entity_decode($query);
            $keyvaluepairs = explode('&', $query);
            if ($keyvaluepairs !== false) {
                foreach($keyvaluepairs as $keyvaluepair) {
                    if (strstr($keyvaluepair, '=') !== false) {
                        list($key, $value) = explode('=', $keyvaluepair);
                        if (strcmp('controller', $key) === 0) {
                            $controller = $value;
                        }
                        else if (strcmp('fc', $key) === 0) {
                            $is_fc_module = strcmp('module', $value) !== false;
                        }
                    }
                }
            }
        }
        if (!Validate::isControllerName($controller))
            $controller = false;
        if (!$controller && $this->use_routes) {
            $url_without_lang = $url;
            if (isset($this->routes[$id_shop][Context::getContext()->language->id])) {
                foreach ($this->routes[$id_shop][Context::getContext()->language->id] as $route) {
                    if (preg_match($route['regexp'], $url_without_lang, $m)) {
                        $controller = $route['controller'] ? $route['controller'] : false;
                        if (preg_match('#module-([a-z0-9_-]+)-([a-z0-9_]+)$#i', $controller, $m)) {
                            $controller = $m[2];
                        }
                        if ($is_fc_module)
                            $controller = false;
                        break;
                    }
                }
            }
            if (!$controller && Tools::strlen($url_without_lang) == 0) {
                $controller = 'index';
            }
            else if ($controller == 'index' || preg_match('/^\/index.php(?:\?.*)?$/', $url_without_lang)) {
                if ($is_fc_module) {
                    $controller = false;
                }
            }
        }
        return $controller;
    }
    public function dispatch() {
        if (Tools::version_compare(_PS_VERSION_,'1.6','>')) {
            $this->dispatch_16();
        } else {
            $this->dispatch_15();
        }
    }
    private function dispatch_15()
    {
        $this->getController();
        if (!$this->controller)
            $this->controller = $this->default_controller;
        $this->page_cache_start_time = microtime(true);
        if ($this->_isPageCacheActive())
        {
            $pre_display_html = PageCache::preDisplayStats();
            $cache_file = PageCache::getCacheFile();
            if ($cache_file !== false)
            {
                PageCache::readfile($cache_file);
                PageCache::displayStats(true, $pre_display_html);
                return;
            }
        }
        $controller_class = '';
        switch ($this->front_controller)
        {
            case self::FC_FRONT :
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_.'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth']))
                    $controllers['authentication'] = $controllers['auth'];
                if (isset($controllers['compare']))
                    $controllers['productscomparison'] = $controllers['compare'];
                if (isset($controllers['contact']))
                    $controllers['contactform'] = $controllers['contact'];
                if (!isset($controllers[Tools::strtolower($this->controller)]))
                    $this->controller = $this->controller_not_found;
                $controller_class = $controllers[Tools::strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 0);
            break;
            case self::FC_MODULE :
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active)
                {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                    if (isset($controllers[Tools::strtolower($this->controller)]))
                    {
                        include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                        $controller_class = $module_name.$this->controller.'ModuleFrontController';
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 1);
            break;
            case self::FC_ADMIN :
                $tab = Tab::getInstanceFromClassName($this->controller);
                $retrocompatibility_admin_tab = null;
                if ($tab->module)
                {
                    if (file_exists(_PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php'))
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php';
                    else
                    {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/');
                        if (!isset($controllers[Tools::strtolower($this->controller)]))
                        {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        }
                        else
                        {
                            include_once(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/'.$controllers[Tools::strtolower($this->controller)].'.php');
                            $controller_class = $controllers[Tools::strtolower($this->controller)].(strpos($controllers[Tools::strtolower($this->controller)], 'Controller') ? '' : 'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 1);
                }
                else
                {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_.'/tabs/', _PS_ADMIN_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_.'controllers/admin/'));
                    if (!isset($controllers[Tools::strtolower($this->controller)]))
                        $this->controller = $this->controller_not_found;
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 0);
                    if (file_exists(_PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php'))
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php';
                }
                if ($retrocompatibility_admin_tab)
                {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_.'/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
            break;
            default :
                throw new PrestaShopException('Bad front controller chosen');
        }
        try
        {
            $controller = Controller::getController($controller_class);
            if (isset($params_hook_action_dispatcher))
                Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
            $controller->run();
            if ($this->_isPageCacheActive())
            {
                PageCache::displayStats(false, $pre_display_html);
            }
        }
        catch (PrestaShopException $e)
        {
            $e->displayMessage();
        }
    }
    private function dispatch_16()
    {
        $controller_class = '';
        $this->getController();
        if (!$this->controller) {
            if (!method_exists($this, 'useDefaultController'))
                $this->controller = $this->default_controller;
            else
                $this->controller = $this->useDefaultController();
        }
        $this->page_cache_start_time = microtime(true);
        if ($this->_isPageCacheActive())
        {
            $pre_display_html = PageCache::preDisplayStats();
            $cache_file = PageCache::getCacheFile();
            if ($cache_file !== false)
            {
                PageCache::readfile($cache_file);
                PageCache::displayStats(true, $pre_display_html);
                return;
            }
        }
        switch ($this->front_controller)
        {
            case self::FC_FRONT :
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_.'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth']))
                    $controllers['authentication'] = $controllers['auth'];
                if (isset($controllers['compare']))
                    $controllers['productscomparison'] = $controllers['compare'];
                if (isset($controllers['contact']))
                    $controllers['contactform'] = $controllers['contact'];
                if (!isset($controllers[Tools::strtolower($this->controller)]))
                    $this->controller = $this->controller_not_found;
                $controller_class = $controllers[Tools::strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 0);
                break;
            case self::FC_MODULE :
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active)
                {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                    if (isset($controllers[Tools::strtolower($this->controller)]))
                    {
                        include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                        $controller_class = $module_name.$this->controller.'ModuleFrontController';
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 1);
                break;
            case self::FC_ADMIN :
                if (isset($this->use_default_controller) && !Tools::getValue('token') && Validate::isLoadedObject(Context::getContext()->employee) && Context::getContext()->employee->isLoggedBack())
                    Tools::redirectAdmin('index.php?controller='.$this->controller.'&token='.Tools::getAdminTokenLite($this->controller));
                $tab = Tab::getInstanceFromClassName($this->controller, Configuration::get('PS_LANG_DEFAULT'));
                $retrocompatibility_admin_tab = null;
                if ($tab->module)
                {
                    if (file_exists(_PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php'))
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php';
                    else
                    {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/');
                        if (!isset($controllers[Tools::strtolower($this->controller)]))
                        {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        }
                        else
                        {
                            include_once(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/'.$controllers[Tools::strtolower($this->controller)].'.php');
                            $controller_class = $controllers[Tools::strtolower($this->controller)].(strpos($controllers[Tools::strtolower($this->controller)], 'Controller') ? '' : 'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 1);
                }
                else
                {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_.'/tabs/', _PS_ADMIN_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_.'controllers/admin/'));
                    if (!isset($controllers[Tools::strtolower($this->controller)]))
                    {
                        if (Validate::isLoadedObject($tab) && $tab->id_parent == 0 && ($tabs = Tab::getTabs(Context::getContext()->language->id, $tab->id)) && isset($tabs[0]))
                            Tools::redirectAdmin(Context::getContext()->link->getAdminLink($tabs[0]['class_name']));
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 0);
                    if (file_exists(_PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php'))
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php';
                }
                if ($retrocompatibility_admin_tab)
                {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_.'/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
                break;
            default :
                throw new PrestaShopException('Bad front controller chosen');
        }
        try
        {
			/** old code
            $controller = Controller::getController($controller_class);
				if (isset($params_hook_action_dispatcher))
					Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
				*/
		if (isset($params_hook_action_dispatcher))
			Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
		if ($this->advanced_dispatcher) {
			if ($this->front_controller == self::FC_FRONT) {
				$controller_class = $controllers[Tools::strtolower($this->controller)];
			} elseif ($this->front_controller == self::FC_MODULE) {
				$module_name   =   Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
				$module = Module::getInstanceByName($module_name);
				$controller_class = 'PageNotFoundController';
				if (Validate::isLoadedObject($module) && $module->active) {
					$controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
					if (isset($controllers[Tools::strtolower($this->controller)])) {
						include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
						$controller_class  = $module_name.$this->controller.'ModuleFrontController';
					}	
				}
			}
		}
			$controller = Controller::getController($controller_class);
			$controller = Controller::getController($controller_class);
            $controller->run();
            if ($this->_isPageCacheActive())
            {
                PageCache::displayStats(false, $pre_display_html);
            }
        }
        catch (PrestaShopException $e)
        {
            $e->displayMessage();
        }
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    protected $backup = array();
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    protected $advanced_dispatcher = 0;
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    protected function __construct()
    {
        if (version_compare(_PS_VERSION_, '1.5.2.0', '<=')) {
            $this->default_routes = array(
                'supplier_rule' => array(
                    'controller' => 'supplier',
                    'rule' => 'supplier/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_supplier'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'manufacturer_rule' => array(
                    'controller' => 'manufacturer',
                    'rule' => 'manufacturers/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_manufacturer'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'cms_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'cms_category_rule' => array(
                    'controller' => 'cms',
                    'rule' => 'content/{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_cms_category'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'module' => array(
                    'controller' => null,
                    'rule' => 'module/{module}{/:controller}',
                    'keywords' => array(
                        'module' => array('regexp' => '[_a-zA-Z0-9_-]+', 'param' => 'module'),
                        'controller' => array('regexp' => '[_a-zA-Z0-9_-]+', 'param' => 'controller'),
                    ),
                    'params' => array(
                        'fc' => 'module',
                    ),
                ),
                'category_rule' => array(
                    'controller' => 'category',
                    'rule' => '{rewrite}/',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_category'),
                        'categories' => array('regexp' => '[/_a-zA-Z0-9-\pL]*'),
                        'id' => array('regexp' => '[0-9]+'),
                        'meta_keywords' =>  array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' =>     array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'product_rule' => array(
                    'controller' => 'product',
                    'rule' => '{category:/}{rewrite}.html',
                    'keywords' => array(
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite_product'),
                        'category' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'categories' => array('regexp' => '[/_a-zA-Z0-9-\pL]*'),
                        'id' => array('regexp' => '[0-9]+'),
                        'ean13' => array('regexp' => '[0-9\pL]*'),
                        'reference' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'manufacturer' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'supplier' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'price' => array('regexp' => '[0-9\.,]*'),
                        'tags' => array('regexp' => '[a-zA-Z0-9-\pL]*'),
                    ),
                ),
                'layered_rule' => array(
                    'controller' => 'category',
                    'rule' => '{id}-{rewrite}{/:selected_filters}',
                    'keywords' => array(
                        'id' => array('regexp' => '[0-9]+', 'param' => 'id_category'),
                        'selected_filters' => array('regexp' => '.*', 'param' => 'selected_filters'),
                        'rewrite' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_keywords' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                        'meta_title' => array('regexp' => '[_a-zA-Z0-9-\pL]*'),
                    ),
                ),
            );
        }
        parent::__construct();
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function setUri($u = '')
    {
        $this->request_uri = $u;
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function setController($c = '')
    {
        $this->advanced_dispatcher = $this->controller != $c ? 1 : 0;
        $this->controller = $c;
        $_GET['controller'] = $this->controller;
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function getOldController($r)
    {
        $id_lang = (int)Context::getContext()->language->id;
        $o = version_compare(_PS_VERSION_, '1.5.5.0', '<');
        $b1 = $this->default_routes;
        if ($o) {
            $b2 = $this->routes[$id_lang];
        } else {
            $id_shop = (int)Context::getContext()->shop->id;
            $b2 = $this->routes[$id_shop][$id_lang];
        }
        foreach ($r as $k => $v) {
            unset($this->default_routes[$k]['keywords']['rewrite']['param']);
            $this->default_routes[$k]['keywords']['id']['param'] = 'id_'.$this->default_routes[$k]['controller'];
            $this->addRoute(
                $k,
                $v,
                $this->default_routes[$k]['controller'],
                $id_lang,
                $this->default_routes[$k]['keywords'],
                isset($this->default_routes[$k]['params']) ? $this->default_routes[$k]['params'] : array()
            );
        }
        $this->setController();
        $c = $this->getController();
        $this->default_routes = $b1;
        if ($o) {
            $this->routes[$id_lang] = $b2;
        } else {
            $this->routes[$id_shop][$id_lang] = $b2;
        }
        return $c;
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function removeRoute($route_id, $id_lang = null, $id_shop = null)
    {
        if ($id_lang === null && isset(Context::getContext()->language)) {
            $id_lang = (int)Context::getContext()->language->id;
        }
        if ($id_shop === null && isset(Context::getContext()->shop)) {
            $id_shop = (int)Context::getContext()->shop->id;
        }
        if (isset($this->routes[$id_shop][$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_shop][$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_shop][$id_lang][$route_id.'_rule']);
        } elseif (isset($this->routes[$id_lang][$route_id.'_rule'])) {
            $this->backup[$route_id.'_rule'] = $this->routes[$id_lang][$route_id.'_rule'];
            unset($this->routes[$id_lang][$route_id.'_rule']);
        }
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function restoreRoute()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $layered = null;
        if (isset($this->backup['layered_rule'])) {
            $layered = $this->backup['layered_rule'];
            unset($this->backup['layered_rule']);
        }
        if (isset($this->routes[$id_shop][$id_lang])) {
            $this->routes[$id_shop][$id_lang] = array_merge($this->routes[$id_shop][$id_lang], $this->backup);
            if ($layered) {
                $this->routes[$id_shop][$id_lang]['layered_rule'] = $layered;
            }
        } else {
            $this->routes[$id_lang] = array_merge($this->routes[$id_lang], $this->backup);
            if ($layered) {
                $this->routes[$id_lang]['layered_rule'] = $layered;
            }
        }
        $this->backup = array();
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function dispatch_renamed()
    {
        $o = Configuration::get('VIP_ADVANCED_URL_DISPATCHER');
        if (!$o) {
            parent::dispatch();
            return;
        }
        $controller_class = '';
        $this->getController();
        if (!$this->controller) {
            $this->controller = method_exists($this, 'useDefaultController') ? $this->useDefaultController() :
                $this->default_controller;
        }
        switch ($this->front_controller) {
            case self::FC_FRONT:
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_,
                    _PS_OVERRIDE_DIR_.'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth'])) {
                    $controllers['authentication'] = $controllers['auth'];
                }
                if (isset($controllers['compare'])) {
                    $controllers['productscomparison'] = $controllers['compare'];
                }
                if (isset($controllers['contact'])) {
                    $controllers['contactform'] = $controllers['contact'];
                }
                if (!isset($controllers[Tools::strtolower($this->controller)])) {
                    $this->controller = $this->controller_not_found;
                }
                $controller_class = $controllers[Tools::strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 0);
                break;
            case self::FC_MODULE:
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active) {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                    if (isset($controllers[Tools::strtolower($this->controller)])) {
                        include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                        $controller_class = $module_name.$this->controller.'ModuleFrontController';
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT,
                    'controller_class' => $controller_class, 'is_module' => 1);
                break;
            case self::FC_ADMIN:
                if (isset($this->use_default_controller) && $this->use_default_controller && !Tools::getValue('token')
                    && Validate::isLoadedObject(Context::getContext()->employee)
                    && Context::getContext()->employee->isLoggedBack()) {
                    Tools::redirectAdmin(
                        'index.php?controller='.$this->controller.'&token='.Tools::getAdminTokenLite($this->controller)
                    );
                }
                $tab = Tab::getInstanceFromClassName($this->controller, Configuration::get('PS_LANG_DEFAULT'));
                $retrocompatibility_admin_tab = null;
                if ($tab->module) {
                    if (file_exists(_PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php')) {
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_.$tab->module.'/'.$tab->class_name.'.php';
                    } else {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/');
                        if (!isset($controllers[Tools::strtolower($this->controller)])) {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        } else {
                            include_once(_PS_MODULE_DIR_.$tab->module.'/controllers/admin/'.
                                $controllers[Tools::strtolower($this->controller)].'.php');
                            $controller_class = $controllers[Tools::strtolower($this->controller)]
                                .(strpos($controllers[Tools::strtolower($this->controller)], 'Controller') ? '' :
                                'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 1);
                } else {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_.'/tabs/', _PS_ADMIN_CONTROLLER_DIR_,
                        _PS_OVERRIDE_DIR_.'controllers/admin/'));
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        if (version_compare(_PS_VERSION_, '1.6.0.1', '>=') && Validate::isLoadedObject($tab)
                            && $tab->id_parent == 0
                            && ($tabs = Tab::getTabs(Context::getContext()->language->id, $tab->id))
                            && isset($tabs[0])) {
                            Tools::redirectAdmin(Context::getContext()->link->getAdminLink($tabs[0]['class_name']));
                        }
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN,
                        'controller_class' => $controller_class, 'is_module' => 0);
                    if (file_exists(_PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php')) {
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_.'/tabs/'.$controller_class.'.php';
                    }
                }
                if ($retrocompatibility_admin_tab) {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_.'/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
                break;
            default:
                throw new PrestaShopException('Bad front controller chosen');
        }
        try {
            if (isset($params_hook_action_dispatcher)) {
                Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
            }
            if ($this->advanced_dispatcher) {
                if ($this->front_controller == self::FC_FRONT) {
                    if (!isset($controllers[Tools::strtolower($this->controller)])) {
                        $this->controller = $this->controller_not_found;
                    }
                    $controller_class = $controllers[Tools::strtolower($this->controller)];
                } elseif ($this->front_controller == self::FC_MODULE) {
                    $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                    $module = Module::getInstanceByName($module_name);
                    $controller_class = 'PageNotFoundController';
                    if (Validate::isLoadedObject($module) && $module->active) {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_.$module_name.'/controllers/front/');
                        if (isset($controllers[Tools::strtolower($this->controller)])) {
                            include_once(_PS_MODULE_DIR_.$module_name.'/controllers/front/'.$this->controller.'.php');
                            $controller_class = $module_name.$this->controller.'ModuleFrontController';
                        }
                    }
                }
            }
            $controller = Controller::getController($controller_class);
            $controller->run();
        } catch (PrestaShopException $e) {
            $e->displayMessage();
        }
    }
}