<?php
class Link extends LinkCore
{
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    protected static $category_disable_rewrite = null;
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function __construct($protocol_link = null, $protocol_content = null)
    {
        parent::__construct($protocol_link, $protocol_content);
        if (Link::$category_disable_rewrite === null) {
            Link::$category_disable_rewrite = array(Configuration::get('PS_HOME_CATEGORY'),
                Configuration::get('PS_ROOT_CATEGORY'));
        }
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function getCategoryLink(
        $category,
        $alias = null,
        $id_lang = null,
        $selected_filters = null,
        $id_shop = null,
        $relative_protocol = false
    ) {
        $dispatcher = Dispatcher::getInstance();
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $url = (method_exists($this, 'getBaseLink') ? $this->getBaseLink($id_shop, null, $relative_protocol) :
            _PS_BASE_URL_.__PS_BASE_URI__).$this->getLangLink($id_lang);
        if (!is_object($category)) {
            $category = new Category($category, $id_lang);
        }
        $params = array();
        $params['id'] = $category->id;
        $params['rewrite'] = (!$alias) ? $category->link_rewrite : $alias;
        $params['meta_keywords'] =  Tools::str2url($category->getFieldByLang('meta_keywords'));
        $params['meta_title'] = Tools::str2url($category->getFieldByLang('meta_title'));
        if ($dispatcher->hasKeyword('category_rule', $id_lang, 'categories', $id_shop)) {
            $p = array();
            foreach ($category->getParentsCategories($id_lang) as $c) {
                if (!in_array($c['id_category'], Link::$category_disable_rewrite)
                    && $c['id_category'] != $category->id) {
                    $p[$c['level_depth']] = $c['link_rewrite'];
                }
            }
            $params['categories'] = implode('/', array_reverse($p));
        }
        $selected_filters = is_null($selected_filters) ? '' : $selected_filters;
        if (empty($selected_filters)) {
            $rule = 'category_rule';
        } else {
            $rule = 'layered_rule';
            $params['selected_filters'] = $selected_filters;
        }
        return $url.$dispatcher->createUrl($rule, $id_lang, $params, $this->allow, '', $id_shop);
    }
    /*
    * module: vipadvancedurl
    * date: 2017-08-17 23:59:34
    * version: 1.4.2
    */
    public function getProductLink(
        $product,
        $alias = null,
        $category = null,
        $ean13 = null,
        $id_lang = null,
        $id_shop = null,
        $ipa = 0,
        $force_routes = false,
        $relative_protocol = false,
        $add_anchor = false,
        $extraParams = array()
    ) {
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            return parent::getProductLink(
                $product,
                $alias,
                $category,
                $ean13,
                $id_lang,
                $id_shop,
                $ipa,
                $force_routes,
                $relative_protocol,
                $add_anchor,
                $extraParams
            );
        } elseif (version_compare(_PS_VERSION_, '1.6.1.1', '>=')) {
            return parent::getProductLink(
                $product,
                $alias,
                $category,
                $ean13,
                $id_lang,
                $id_shop,
                $ipa,
                $force_routes,
                $relative_protocol,
                $add_anchor
            );
        }
        $dispatcher = Dispatcher::getInstance();
        if (!$id_lang) {
            $id_lang = Context::getContext()->language->id;
        }
        $url = (method_exists($this, 'getBaseLink') ? $this->getBaseLink($id_shop, null, $relative_protocol) :
            _PS_BASE_URL_.__PS_BASE_URI__).$this->getLangLink($id_lang);
        if (!is_object($product)) {
            if (is_array($product) && isset($product['id_product'])) {
                    $product = new Product($product['id_product'], false, $id_lang);
            } elseif (is_numeric($product) || !$product) {
                $product = new Product($product, false, $id_lang);
            } else {
                throw new PrestaShopException('Invalid product vars');
            }
        }
        $params = array();
        $params['id'] = $product->id;
        $params['rewrite'] = (!$alias) ? $product->getFieldByLang('link_rewrite') : $alias;
        $params['ean13'] = (!$ean13) ? $product->ean13 : $ean13;
        $params['meta_keywords'] =  Tools::str2url($product->getFieldByLang('meta_keywords'));
        $params['meta_title'] = Tools::str2url($product->getFieldByLang('meta_title'));
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'manufacturer')) {
            $params['manufacturer'] = Tools::str2url(
                $product->isFullyLoaded ? $product->manufacturer_name :
                Manufacturer::getNameById($product->id_manufacturer)
            );
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'supplier')) {
            $params['supplier'] = Tools::str2url(
                $product->isFullyLoaded ? $product->supplier_name : Supplier::getNameById($product->id_supplier)
            );
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'price')) {
            $params['price'] = $product->isFullyLoaded ? $product->price : Product::getPriceStatic(
                $product->id,
                false,
                null,
                6,
                null,
                false,
                true,
                1,
                false,
                null,
                null,
                null,
                $product->specificPrice
            );
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'tags')) {
            $params['tags'] = Tools::str2url($product->getTags($id_lang));
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'category')) {
            $params['category'] = (!is_null($product->category) && !empty($product->category)) ?
                Tools::str2url($product->category) : Tools::str2url($category);
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'reference')) {
            $params['reference'] = Tools::str2url($product->reference);
        }
        if ($dispatcher->hasKeyword('product_rule', $id_lang, 'categories')) {
            $params['category'] = (!$category) ? $product->category : $category;
            $cats = array();
            foreach ($product->getParentCategories($id_lang) as $cat) {
                if (!in_array($cat['id_category'], Link::$category_disable_rewrite)) {
                    $cats[] = $cat['link_rewrite'];
                }
            }
            $params['categories'] = implode('/', $cats);
        }
        $anchor = $ipa ? $product->getAnchor($ipa) : '';
        return $url.$dispatcher->createUrl('product_rule', $id_lang, $params, $force_routes, $anchor);
    }
    /*
    * module: allegro
    * date: 2017-09-27 22:52:04
    * version: 4.1.0.8
    */
    public function getImageLink($name, $ids, $type = null)
    {
        if (Configuration::get('ALLEGRO_LEGACY_IMAGES') 
            && Configuration::get('PS_LEGACY_IMAGES') 
            && strpos($ids, '-') === false) {
            $image = new Image((int)$ids);
            $ids = $image->id_product.'-'.$ids;
        }
        return parent::getImageLink($name, $ids, $type);
    }
}
