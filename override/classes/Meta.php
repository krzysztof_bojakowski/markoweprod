<?php
class Meta extends MetaCore
{
				
	/**
     * Get category meta tags
     *
     * @since 1.5.0
     * @param int $id_category
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getCategoryMetas($id_category, $id_lang, $page_name, $title = '')
    {
        if (!empty($title)) {
            $title = ' - '.$title;
        }
        $page_number = (int)Tools::getValue('p');
        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`, `description`
				FROM `'._DB_PREFIX_.'category_lang` cl
				WHERE cl.`id_lang` = '.(int)$id_lang.'
					AND cl.`id_category` = '.(int)$id_category.Shop::addSqlRestrictionOnLang('cl');

        $cache_id = 'Meta::getCategoryMetas'.(int)$id_category.'-'.(int)$id_lang;
        if (!Cache::isStored($cache_id)) {
            if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
                if (empty($row['meta_description'])) {
                    $row['meta_description'] = strip_tags($row['description']) . (!empty($page_number) ? ' ('.$page_number.')' : '');
                } else {
					$row['meta_description'] = $row['meta_description'] . (!empty($page_number) ? ' ('.$page_number.')' : '');
				}

                // Paginate title
                if (!empty($row['meta_title'])) {
                    $row['meta_title'] = $title.$row['meta_title'].(!empty($page_number) ? ' ('.$page_number.')' : '').' - '.Configuration::get('PS_SHOP_NAME');
                } else {
                    $row['meta_title'] = $row['name'].(!empty($page_number) ? ' ('.$page_number.')' : '').' - '.Configuration::get('PS_SHOP_NAME');
                }

                if (!empty($title)) {
                    $row['meta_title'] = $title.(!empty($page_number) ? ' ('.$page_number.')' : '').' - '.Configuration::get('PS_SHOP_NAME');
                }

                $result = Meta::completeMetaTags($row, $row['name']);
            } else {
                $result = Meta::getHomeMetas($id_lang, $page_name);
            }
            Cache::store($cache_id, $result);
            return $result;
        }
        return Cache::retrieve($cache_id);
    }	
	
    /**
     * Get manufacturer meta tags
     *
     * @since 1.5.0
     * @param int $id_manufacturer
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
    public static function getManufacturerMetas($id_manufacturer, $id_lang, $page_name)
    {
        $page_number = (int)Tools::getValue('p');
        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`
				FROM `'._DB_PREFIX_.'manufacturer_lang` ml
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (ml.`id_manufacturer` = m.`id_manufacturer`)
				WHERE ml.id_lang = '.(int)$id_lang.'
					AND ml.id_manufacturer = '.(int)$id_manufacturer;
        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            if (empty($row['meta_description'])) {
                $row['meta_description'] = strip_tags($row['description']) . (!empty($page_number) ? ' ('.$page_number.')' : '');
            } else {
				$row['meta_description'] = $row['meta_description'] . (!empty($page_number) ? ' ('.$page_number.')' : '');
			}
            $row['meta_title'] = ($row['meta_title'] ? $row['meta_title'] : $row['name']).(!empty($page_number) ? ' ('.$page_number.')' : '');
            $row['meta_title'] .= ' - '.Configuration::get('PS_SHOP_NAME');
            return Meta::completeMetaTags($row, $row['meta_title']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }
	
	    /**
     * Get meta tags for a given page
     *
     * @since 1.5.0
     * @param int $id_lang
     * @param string $page_name
     * @return array Meta tags
     */
    public static function getHomeMetas($id_lang, $page_name)
    {
		$page_number = (int)Tools::getValue('p');
        $metas = Meta::getMetaByPage($page_name, $id_lang);
        $ret['meta_title'] = (isset($metas['title']) && $metas['title']) ? $metas['title'].' - '.Configuration::get('PS_SHOP_NAME') . (!empty($page_number) ? ' ('.$page_number.')' : '')  : Configuration::get('PS_SHOP_NAME') . (!empty($page_number) ? ' ('.$page_number.')' : '');
        $ret['meta_description'] = (isset($metas['description']) && $metas['description']) ? $metas['description'] : '';
        $ret['meta_keywords'] = (isset($metas['keywords']) && $metas['keywords']) ? $metas['keywords'] :  '';
        return $ret;
    }

}