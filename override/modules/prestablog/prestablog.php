<?php
/**
 * 2017 Paweł Wawrzyniak
 *
 * MODULE PrestaBlog extends
 *
 * @version   3.6.8
 * @author    Paweł Wawrzyniak <pawel.adam.wawrzyniak@gmail.com>
 * @description module add page to meta_description if pagination is avaible
 *
 */

if (!defined('_PS_VERSION_'))
	exit;

class PrestaBlogOverride extends PrestaBlog
{
	public static function completeMetaTags($meta_tags, $meta_title = null, $meta_description = null)
	{
		$context = Context::getContext();

		$prestablog = new PrestaBlog();

		if (empty($meta_tags['meta_title']))
			$meta_tags['meta_title'] = ($meta_title ? $meta_title.' - ' : '').Configuration::get('PS_SHOP_NAME');
		if (empty($meta_tags['meta_description']))
			$meta_tags['meta_description'] = ($meta_description ? $meta_description : '');
		if (empty($meta_tags['meta_keywords']))
			$meta_tags['meta_keywords'] = Configuration::get('PS_META_KEYWORDS', (int)$context->language->id) ?
		Configuration::get('PS_META_KEYWORDS', (int)$context->language->id) : '';
		
		$meta_tags['meta_title'] .= (Tools::getValue('p') ? ' -- '.$prestablog->l('page').' '.Tools::getValue('p') : '');
		$meta_tags['meta_title'] .= (Tools::getValue('y') ? ' - '.Tools::getValue('y') : '');
		$meta_tags['meta_title'] .= (Tools::getValue('m') ? ' - '.$prestablog->mois_langue[Tools::getValue('m')] : '');
		$meta_tags['meta_description'] .= (Tools::getValue('p') ? ' - '.$prestablog->l('page').' '.Tools::getValue('p') : ''); 

		return $meta_tags;
	}
}

?>