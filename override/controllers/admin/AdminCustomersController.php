<?php
/**
 * NOTICE OF LICENSE
 *
 * @author    Benhauer Sp. z o.o. info@benhauer.pl
 * @copyright Copyright (c) 2015 Benhauer Sp. z o.o.
 * @license   https://opensource.org/licenses/MIT The MIT License (MIT)
 */
class AdminCustomersController extends AdminCustomersControllerCore
{
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:06
    * version: 1.6.7
    */
    protected function doPostRequestSm($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array('Content-Type: application/json',
                'Content-Length: ' . Tools::strlen($data)
            )
        );
        return curl_exec($ch);
    }
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:06
    * version: 1.6.7
    */
    public function processChangeNewsletterVal()
    {
        $clientId = Configuration::get('SM_CLIENT_ID');
        $endpoint = Configuration::get('SM_ENDPOINT');
        $owner = Configuration::get('SM_EMAIL');
        $apiSecret = Configuration::get('SM_API');
        $adminTags = explode(",", Configuration::get('SM_ADMIN_EDIT_TAGS'));
        $newsletterTag =  explode(",", Configuration::get('SM_NEWSLETTER_TAGS'));
        $apiKey = 'j2q8qp4fbp9qf2b8p49fb'; //Random string
        $tagsToAdd = $adminTags;
        $customer = new Customer($this->id_object);
        $email = $customer->email;
        $newsletter = $customer->newsletter;
        $birthday = $customer->birthday;
        if (!$newsletter) {
            $optOutForce = 'false';
            $optInForce = 'true';
            $tagsToAdd = array_merge($tagsToAdd, $newsletterTag);
        } else {
            $optOutForce = 'true';
            $optInForce = 'false';
            $tagToDelete = $newsletterTag;
        }
        if ($birthday[6] == '-') {
            $birthday = substr_replace($birthday, '0', 5, 0);
        }
        if (Tools::strlen($birthday) == 9) {
            $birthday = substr_replace($birthday, '0', 8, 0);
        }
        $birthday = str_replace('-', '', $birthday);
        $data = array(
            'clientId' => $clientId,
            'apiKey' => $apiKey,
            'requestTime' => time(),
            'sha' => sha1($apiKey . $clientId . $apiSecret),
            'contact' => array(
                'email' => $email,
            ),
            'owner' => $owner,
            'forceOptOut' => $optOutForce,
            'forceOptIn' => $optInForce,
            'tags' => $tagsToAdd,
            'removeTags' => $tagToDelete,
            'birthday' => $birthday,
        );
        $json = Tools::jsonEncode($data);
        $this->doPostRequestSm('http://' . $endpoint . '/api/contact/upsert', $json);
        parent::processChangeNewsletterVal();
    }
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:06
    * version: 1.6.7
    */
    public function processChangeOptinVal()
    {
        $clientId = Configuration::get('SM_CLIENT_ID');
        $endpoint = Configuration::get('SM_ENDPOINT');
        $owner = Configuration::get('SM_EMAIL');
        $apiSecret = Configuration::get('SM_API');
        $adminTags = explode(",", Configuration::get('SM_ADMIN_EDIT_TAGS'));
        $mailingListTags = explode(",", Configuration::get('SM_MAILING_LIST'));
        $apiKey = 'j2q8qp4fbp9qf2b8p49fb'; //Random string
        $customer = new Customer($this->id_object);
        $email = $customer->email;
        $optIn = $customer->optin;
        $tagsToDelete = array();
        $birthday = $customer->birthday;
        $tagsToAdd = $adminTags;
        if (!$optIn) {
            $tagsToAdd = array_merge($tagsToAdd, $mailingListTags);
        } else {
            $tagsToDelete = $mailingListTags;
        }
        if ($birthday[6] == '-') {
            $birthday = substr_replace($birthday, '0', 5, 0);
        }
        if (Tools::strlen($birthday) == 9) {
            $birthday = substr_replace($birthday, '0', 8, 0);
        }
        $birthday = str_replace('-', '', $birthday);
        $data = array(
            'clientId' => $clientId,
            'apiKey' => $apiKey,
            'requestTime' => time(),
            'sha' => sha1($apiKey . $clientId . $apiSecret),
            'contact' => array(
                'email' => $email,
            ),
            'owner' => $owner,
            'tags' => $tagsToAdd,
            'removeTags' => $tagsToDelete,
            'birthday' => $birthday,
        );
        $json = Tools::jsonEncode($data);
        $this->doPostRequestSm('http://' . $endpoint . '/api/contact/upsert', $json);
        parent::processChangeOptinVal();
    }
}
