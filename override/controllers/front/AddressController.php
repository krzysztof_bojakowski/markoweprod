<?php
/**
 * NOTICE OF LICENSE
 *
 * @author    Benhauer Sp. z o.o. info@benhauer.pl
 * @copyright Copyright (c) 2015 Benhauer Sp. z o.o.
 * @license   https://opensource.org/licenses/MIT The MIT License (MIT)
 */
class AddressController extends AddressControllerCore
{
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:06
    * version: 1.6.7
    */
    protected function doPostRequestSm($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . Tools::strlen($data)
            )
        );
        return curl_exec($ch);
    }
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:06
    * version: 1.6.7
    */
    protected function processSubmitAddress()
    {
        $clientId = Configuration::get('SM_CLIENT_ID');
        $endpoint = Configuration::get('SM_ENDPOINT');
        $owner = Configuration::get('SM_EMAIL');
        $apiSecret = Configuration::get('SM_API');
        $apiKey = 'j2q8qp4fbp9qf2b8p49fb';
        $company = Tools::getValue('company');
        $street1 = Tools::getValue('address1');
        $street2 = Tools::getValue('address2');
        $street = $street1 . ' ' . $street2;
        $postalCode = Tools::getValue('postcode');
        $city = Tools::getValue('city');
        $country = Country::getIsoById(Tools::getValue('id_country'));
        $mobilePhone = Tools::getValue('phone_mobile');
        $landLinePhone = Tools::getValue('phone');
        $email = $this->context->customer->email;
        $firstName = $this->context->customer->firstname;
        $lastName = $this->context->customer->lastname;
        $name = $firstName . " " . $lastName;
        $birthday = $this->context->customer->birthday;
        if ($birthday[6] == '-') {
            $birthday = substr_replace($birthday, '0', 5, 0);
        }
        if (Tools::strlen($birthday) == 9) {
            $birthday = substr_replace($birthday, '0', 8, 0);
        }
        $birthday = str_replace('-', '', $birthday);
        $data = array(
            'clientId' => $clientId,
            'apiKey' => $apiKey,
            'requestTime' => time(),
            'sha' => sha1($apiKey . $clientId . $apiSecret),
            'contact' => array(
                'email' => $email,
                'name' => $name,
                'company' => $company,
                'phone' => $mobilePhone,
                'address' => array(
                    'streetAddress' => $street,
                    'zipCode' => $postalCode,
                    'city' => $city,
                    'country' => $country,
                ),
            ),
            'owner' => $owner,
            'birthday' => $birthday,
            'properties' => array(
                'landLine' => $landLinePhone
            ),
        );
        $json = Tools::jsonEncode($data);
        $this->doPostRequestSm('http://' . $endpoint . '/api/contact/upsert', $json);
        parent::processSubmitAddress();
    }
}
