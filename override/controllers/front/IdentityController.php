<?php
/**
 * NOTICE OF LICENSE
 *
 * @author    Benhauer Sp. z o.o. info@benhauer.pl
 * @copyright Copyright (c) 2015 Benhauer Sp. z o.o.
 * @license   https://opensource.org/licenses/MIT The MIT License (MIT)
 */
class IdentityController extends IdentityControllerCore
{
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:07
    * version: 1.6.7
    */
    protected function doPostRequestSm($url, $data)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . Tools::strlen($data)
            )
        );
        return curl_exec($ch);
    }
    /*
    * module: salesmanago
    * date: 2018-02-05 18:47:07
    * version: 1.6.7
    */
    public function postProcess()
    {
        parent::postProcess();
        if (Tools::isSubmit('submitIdentity')) {
            $clientId = Configuration::get('SM_CLIENT_ID');
            $endpoint = Configuration::get('SM_ENDPOINT');
            $owner = Configuration::get('SM_EMAIL');
            $apiSecret = Configuration::get('SM_API');
            $newsletterTag = explode(",", Configuration::get('SM_NEWSLETTER_TAGS'));
            $mailingListTags = explode(",", Configuration::get('SM_MAILING_LIST'));
            $apiKey = 'j2q8qp4fbp9qf2b8p49fb';
            $firstName = $this->customer->firstname;
            $surname = $this->customer->lastname;
            $name = $firstName . " " . $surname;
            $birthday = $this->customer->birthday;
            $newsletterSm = Tools::getIsset('newsletter');
            $optIn = $this->customer->optin;
            $gender = Tools::getValue('id_gender');
            $tagsToAdd = array();
            $tagsToDelete = array();
            $newEmail = $this->customer->email;
            $email = $this->context->cookie->email;
            if ($gender == 1) {
                array_push($tagsToAdd, 'sex_male');
                array_push($tagsToDelete, 'sex_female');
            }
            if ($gender == 2) {
                array_push($tagsToDelete, 'sex_male');
                array_push($tagsToAdd, 'sex_female');
            }
            if ($birthday[6] == '-') {
                $birthday = substr_replace($birthday, '0', 5, 0);
            }
            if (Tools::strlen($birthday) == 9) {
                $birthday = substr_replace($birthday, '0', 8, 0);
            }
            $birthday = str_replace('-', '', $birthday);
            if ($newsletterSm) {
                $optOutForce = 'false';
                $optInForce = 'true';
            } else {
                $optOutForce = 'true';
                $optInForce = 'false';
            }
            if ($newsletterSm) {
                $tagsToAdd = array_merge($tagsToAdd, $newsletterTag);
            } else {
                $tagsToDelete = array_merge($tagsToDelete, $newsletterTag);
            }
            if ($optIn) {
                $tagsToAdd = array_merge($tagsToAdd, $mailingListTags);
            } else {
                $tagsToDelete = array_merge($tagsToDelete, $mailingListTags);
            }
            $data = array(
                'clientId' => $clientId,
                'apiKey' => $apiKey,
                'requestTime' => time(),
                'sha' => sha1($apiKey . $clientId . $apiSecret),
                'contact' => array(
                    'email' => $email,
                    'name' => $name,
                ),
                'owner' => $owner,
                'newEmail' => $newEmail,
                'forceOptOut' => $optOutForce,
                'forceOptIn' => $optInForce,
                'tags' =>  $tagsToAdd,
                'removeTags' => $tagsToDelete,
                'birthday' => $birthday,
            );
            $json1 = Tools::jsonEncode($data);
            $this->doPostRequestSm('http://' . $endpoint . '/api/contact/upsert', $json1);
        }
    }
}
