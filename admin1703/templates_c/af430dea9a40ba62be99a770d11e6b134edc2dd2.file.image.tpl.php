<?php /* Smarty version Smarty-3.1.19, created on 2019-01-09 13:07:28
         compiled from "/var/www/html/modules/allegro/views/theme/image.tpl" */ ?>
<?php /*%%SmartyHeaderCode:24024328059cd1547366142-13786938%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'af430dea9a40ba62be99a770d11e6b134edc2dd2' => 
    array (
      0 => '/var/www/html/modules/allegro/views/theme/image.tpl',
      1 => 1546126547,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '24024328059cd1547366142-13786938',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_59cd15473d22e5_16476069',
  'variables' => 
  array (
    'index' => 0,
    'type' => 0,
    'type_link' => 0,
    'images' => 0,
    'key' => 0,
    'image' => 0,
    'product' => 0,
    'link' => 0,
    'allegro_img_url' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_59cd15473d22e5_16476069')) {function content_59cd15473d22e5_16476069($_smarty_tpl) {?>
<?php if (!isset($_smarty_tpl->tpl_vars['index']->value)) {?><?php $_smarty_tpl->tpl_vars["index"] = new Smarty_variable(0, null, 0);?><?php }?>
<?php if (!isset($_smarty_tpl->tpl_vars['type']->value)) {?><?php $_smarty_tpl->tpl_vars["type"] = new Smarty_variable("medium_default", null, 0);?><?php }?>
<?php if (!isset($_smarty_tpl->tpl_vars['type_link']->value)) {?><?php $_smarty_tpl->tpl_vars["type_link"] = new Smarty_variable("large_default", null, 0);?><?php }?>

<?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['image']->key;
?>
    <?php if ($_smarty_tpl->tpl_vars['index']->value==$_smarty_tpl->tpl_vars['key']->value) {?>
        <?php if ($_smarty_tpl->tpl_vars['image']->value['src']=='shop') {?>
            <?php if (isset($_smarty_tpl->tpl_vars['type_link']->value)) {?>
            <a class="at-image-link" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite,$_smarty_tpl->tpl_vars['image']->value['id'],$_smarty_tpl->tpl_vars['type_link']->value);?>
">
            <?php }?>
                <img class="at-image" src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['product']->value->link_rewrite,$_smarty_tpl->tpl_vars['image']->value['id'],$_smarty_tpl->tpl_vars['type']->value);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" />
        	<?php if (isset($_smarty_tpl->tpl_vars['type_link']->value)) {?>
            </a>
            <?php }?>
        <?php } else { ?>
            <?php if (isset($_smarty_tpl->tpl_vars['type_link']->value)) {?>
            <a class="at-image-link" href="<?php echo $_smarty_tpl->tpl_vars['allegro_img_url']->value;?>
<?php echo intval($_smarty_tpl->tpl_vars['image']->value['id']);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type_link']->value, ENT_QUOTES, 'UTF-8', true);?>
.jpg">
            <?php }?>
                <img class="at-image" src="<?php echo $_smarty_tpl->tpl_vars['allegro_img_url']->value;?>
<?php echo intval($_smarty_tpl->tpl_vars['image']->value['id']);?>
-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['type']->value, ENT_QUOTES, 'UTF-8', true);?>
.jpg" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value->name, ENT_QUOTES, 'UTF-8', true);?>
" />
        	<?php if (isset($_smarty_tpl->tpl_vars['type_link']->value)) {?>
            </a>
            <?php }?>
        <?php }?>
    <?php }?>
<?php } ?>
<?php }} ?>
